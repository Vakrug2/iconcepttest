<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use function auth;
use function view;

class LoginController extends Controller
{
    public function index() {
        $loggedUser = auth()->user();
        
        $registeredUsers = User::query()->orderBy('id')->get();
        
        return view('login', [
            'loggedUser' => $loggedUser,
            'registeredUsers' => $registeredUsers
        ]);
    }
    
    public function register(Request $request) {
        $name = $request->get('name');
        $user = User::create(['name' => $name, 'email' => $name . '@ooo.lv', 'password' => '123']);
        auth()->login($user);
        return redirect()->route('login');
    }
    
    public function loginas(Request $request) {
        $id = $request->get('id');
        $user = User::query()->where(['id' => $id])->firstOrFail();
        auth()->login($user);
        return redirect()->route('login');
    }
    
    public function logout() {
        auth()->logout();
        return redirect()->route('login');
    }
    
    public function token() {
        $token = auth()->user()->createToken('default');
 
        return ['token' => $token->plainTextToken];
    }
}