<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductAttribute;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index() {
        return Product::query()->orderBy('id')->get();
    }
    
    public function new(Request $request) {        
        try {
            DB::beginTransaction();
            $product = new Product();
            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->save();

            foreach ($request->get('attributes') as $attribute) {
                $productAttribute = new ProductAttribute();
                $productAttribute->product_id = $product->id;
                $productAttribute->key = $attribute['key'];
                $productAttribute->value = $attribute['value'];
                $productAttribute->save();
            }
            DB::commit();

            return ['id' => $product->id];
        } catch (Exception $e) {
            DB::rollBack();
            return ['success' => false];
        }
    }
    
    public function get($id) {
        return Product::query()->where(['id' => $id])->with('attributes')->firstOrFail();
    }
    
    public function delete($id) {
        Product::find($id)->delete();
        
        return ['success' => true];
    }
}
