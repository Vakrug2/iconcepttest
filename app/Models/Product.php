<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property ProductAttribute[] $attributes
 */
class Product extends Model
{
    use HasFactory;
    
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }
    
    public static function boot() {
        parent::boot();
        self::deleting(function(Product $product) {
            $product->attributes()->each(function(ProductAttribute $attribute) {
                $attribute->delete();
            });
        });
    }
}
