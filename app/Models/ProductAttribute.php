<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $product_id
 * @property string $key
 * @property string $value
 * @property Product $product
 */
class ProductAttribute extends Model
{
    use HasFactory;
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
