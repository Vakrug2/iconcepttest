<?php

use Illuminate\Foundation\Auth\User;

/** @var User $loggedUser */
/** @var User[] $registeredUsers */
?>

<div>
@if ($loggedUser)
    <div>You are logged in as {{ $loggedUser->name }}.</div>
    <div><a href="{{ route('token') }}">Click to get your access token.</a></div>
@else
    Not logged in!
@endif
</div>
<hr />

<div>
    <div>Click to log in as:</div>
@forelse($registeredUsers as $user)
    <div>
        {{ $user->name }}
        <form style="display: inline-block" method="POST" action="{{ route('loginas') }}">
            @csrf
            <input type="hidden" name="id" value="{{ $user->id }}" />
            <input type="submit" value="Login" />
        </form>
    </div>
@empty
    <p>No registered users</p>
@endforelse
</div>
<hr />

<div>
    <div>Register new user</div>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <input type="text" name="name" />
        <input type="submit" />
    </form>
</div>
<hr />

<a href="{{ route('logout') }}">Logout</a>